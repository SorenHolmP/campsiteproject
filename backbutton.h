#ifndef BACKBUTTON_H
#define BACKBUTTON_H

#include <QPushButton>


class BackButton : public QPushButton
{

public:
    BackButton(const QString &text, QWidget* parent = 0);
    BackButton();
};

#endif // BACKBUTTON_H
