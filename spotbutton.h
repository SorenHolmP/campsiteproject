#ifndef SPOT_BUTTON_H
#define SPOT_BUTTON_H

#include <QPushButton>
#include <QString>


class SpotButton : public QPushButton
{
    Q_OBJECT
public:
    SpotButton(QString text, int id, QWidget* parent = 0);
    SpotButton(int id, QWidget * parent = 0);
    void updateStatusColor(bool booked);
    int id_;

signals:
    void clickedOverload(int id);

private slots:
    void customSlot();
};

#endif
