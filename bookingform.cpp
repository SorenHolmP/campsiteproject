#include "bookingform.h"
#include "ui_bookingform.h"

BookingForm::BookingForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BookingForm)
{
    ui->setupUi(this);
    ui->dateEditFrom->setCalendarPopup(true);
    ui->dateEditTo->setCalendarPopup(true);
    setWindowTitle("Booking form");

}

BookingForm::~BookingForm()
{
    delete ui;
}

void BookingForm::on_buttonBox_accepted()
{
    QDate from = ui->dateEditFrom->date();
    QDate to = ui->dateEditTo->date();
    QString visitor = ui->visitorLineEdit->text();
    emit bookingComplete(from, to, visitor);
}
