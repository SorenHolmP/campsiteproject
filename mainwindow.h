#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include "bookingform.h"
#include "spotbutton.h"
#include "dbmanager.h"
#include <memory>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_spotsOverviewButton_clicked();
    void on_calendarWidget_clicked(const QDate &date);

    void on_pushButton_2_clicked();
    void on_bookingComplete(QDate from, QDate to, QString visitor);
    void on_spotButtonClicked(int id);

    void on_removeSpotsButton_clicked();

private:
    Ui::MainWindow *ui;
    BookingForm* form;
    QVector<SpotButton*> spot_buttons;
    std::unique_ptr<DBManager> dbm_;

};
#endif // MAINWINDOW_H
