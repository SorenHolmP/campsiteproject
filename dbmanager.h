#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QString>
#include <QVector>
#include <booking.h>
#include <spot.h>

class DBManager
{
public:
    DBManager(QString path_to_db);

    //Handle spots

    bool insertSpot(int spot_number);
    bool removeSpot(int spot_number);
    bool spotExists(int spot_number);
    QVector<Spot> getCurrentSpots();


    //Handle current_bookings
    bool insertBooking(const Booking& booking);
    bool removeBooking(int booking_id);
    QVector<Booking> getCurrentBookings();


    //Handle pastBookings



private:
    QSqlDatabase _db;
    bool databaseAlreadyExists();
    void createTables();

    //current bookings
public:
    bool spotIsAvailableInPeriod(int spot_number, QDate start_date, QDate end_date);

};

#endif // DBMANAGER_H
