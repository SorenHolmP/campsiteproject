#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "backbutton.h"
#include <QDebug>
#include <QTextCharFormat>
#include <QDate>
#include <QString>
#include <QVBoxLayout>

#include "bookingform.h"
#include "spotbutton.h"

const int NUM_ROWS = 7;
const int NUM_COLS = 7;


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    dbm_ = std::unique_ptr<DBManager>(new DBManager("camp.db"));

    connect(ui->pushButton, &QPushButton::clicked, this, [this](){ui->stackedWidget->setCurrentIndex(0);});
//    for(int i = 0; i < 10; i++)
//        for(int j = 0; j < 6; j++)
//        {
//            int num = i*10 + j;
//            SpotButton* btn = new SpotButton(QString::number(num), num, this);
//            spot_buttons.push_back(btn);
//            connect(btn, &SpotButton::clickedOverload, this, &MainWindow::on_spotButtonClicked);

//            if(num % 3 == 0)
//                btn->setStyleSheet("background-color:red;");

//            ui->gridLayout->addWidget(btn,i,j);
//        }

    ui->pushButton->setIcon(QIcon(":/kappa/icons/back_button.png"));
    ui->toDateEdit->setDate(QDate::currentDate());
    ui->fromDateEdit->setDate(QDate::currentDate());


    QVector<Spot> spots = dbm_->getCurrentSpots();
    bool available;
    QDate from = ui->fromDateEdit->date();
    QDate to = ui->toDateEdit->date();

    for(const auto &spot : spots)
    {
        available = dbm_->spotIsAvailableInPeriod(spot.id_, from, to);
        SpotButton* btn = new SpotButton(spot.id_);
        btn->updateStatusColor(available);
        spot_buttons.push_back(btn);
    }

    int idx = 0;
    for(int i = 0; i < NUM_ROWS; i++)
    {
        for(int j = 0; j < NUM_COLS; j++)
        {
            ui->spotButtonsGridLayout->addWidget(spot_buttons[idx],i,j);
            idx++;
        }
    }

    QWidget* widget = new QWidget;
    QGridLayout* gridLayout = new QGridLayout;
    widget->setLayout(gridLayout);
    widget->layout()->addWidget(new QPushButton("omgi"));
//    ui->page->layout()->
    QVBoxLayout* verticalLayout = qobject_cast<QVBoxLayout*>(ui->page->layout());
    verticalLayout->insertWidget(0,widget);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_spotsOverviewButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}


void MainWindow::on_calendarWidget_clicked(const QDate &date)
{
    qDebug() << date;
    QTextCharFormat format;
    format.setBackground(QBrush(Qt::red));
    ui->calendarWidget->setDateTextFormat(date,format);

}

void MainWindow::on_pushButton_2_clicked()
{
    form = new BookingForm;
    connect(form, &BookingForm::bookingComplete, this, &MainWindow::on_bookingComplete);
    form->show();
}


void MainWindow::on_bookingComplete(QDate from, QDate to, QString visitor)
{
    qDebug() << from << to << visitor;
    form->hide();
    delete form;
}

void MainWindow::on_spotButtonClicked(int id)
{
    QObject* obj = QObject::sender();
    qDebug() << id;
    SpotButton* btn = qobject_cast<SpotButton*>(obj);
    qDebug() << btn->id_;
}

void MainWindow::on_removeSpotsButton_clicked()
{

    for(auto const& btn : spot_buttons)
    {
        qDebug() << btn->id_;
        ui->gridLayout->removeWidget(btn);
        delete btn;
    }

    spot_buttons.erase(spot_buttons.begin(), spot_buttons.end());
}
