#include "spotbuttonsgrid.h"


SpotButtonsGrid::SpotButtonsGrid()
{

}

SpotButtonsGrid::SpotButtonsGrid(DBManager *dbm, QWidget *parent) :
    dbm_(dbm)
{
    setParent(parent);
    layout = new QGridLayout;
    setLayout(layout);
}


SpotButtonsGrid::~SpotButtonsGrid()
{
    delete layout;
}
