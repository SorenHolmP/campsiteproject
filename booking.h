#ifndef BOOKING_H
#define BOOKING_H

#include <QDate>
#include <QString>


class Booking
{
public:
    Booking();
    Booking(int, QString, QDate, QDate);

public:
    int spot_number_;
    QString visitor_name_;
    QDate start_date_;
    QDate end_date_;
};


#endif // BOOKING_H
