
#include "spotbutton.h"
#include <QDebug>
#include <QString>


SpotButton::SpotButton(QString text, int id, QWidget* parent):
    id_(id)
{
    this->setText(text);
    this->setParent(parent);
    connect(this, &SpotButton::clicked, this, &SpotButton::customSlot);
}

SpotButton::SpotButton(int id, QWidget *parent):
    id_(id)
{
    setParent(parent);
    this->setText(QString::number(id));
}

void SpotButton::updateStatusColor(bool booked)
{
    if(booked)
        setStyleSheet("background-color: red;");
    else
        setStyleSheet("background-color: green;");
}

void SpotButton::customSlot()
{
    emit clickedOverload(id_);
}
