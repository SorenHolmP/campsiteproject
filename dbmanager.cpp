#include "dbmanager.h"
#include <QSqlError>
#include <QDebug>
#include <QFile>
#include <exception>

DBManager::DBManager(QString path_to_db)
{
    _db = QSqlDatabase::addDatabase("QSQLITE");
    _db.setDatabaseName(path_to_db);

    if(!_db.open())
        qDebug() << "Failed to open database";

    if(!databaseAlreadyExists())
    {
        createTables();
    }

}

//Setup
bool DBManager::databaseAlreadyExists()
{
    return _db.tables().contains("spots");
}

void DBManager::createTables()
{
    QSqlQuery query;
    query.prepare("create table spots(spot_number INTEGER PRIMARY KEY)");
    if(!query.exec())
        qDebug() << "Failed to create table spots: " << query.lastError();


    query.prepare("create table bookings(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                  " spot_number INTEGER, start_date TEXT, end_date TEXT, visitor_name TEXT)");
    if(!query.exec())
        qDebug() << "Failed to create table bookings: " << query.lastError();
}

//Manage table "spots"

bool DBManager::insertSpot(int spot_number)
{
    QSqlQuery query;
    query.prepare("INSERT into spots (spot_number) VALUES (:spot_number)");
    query.bindValue(":spot_number", spot_number);
    bool success = query.exec();

    if(!success)
        qDebug() << "Failed to insert spot: " << query.lastError();

    return success;
}

bool DBManager::spotExists(int spot_number)
{
    QSqlQuery query;
    query.prepare("select * from spots where spot_number == (:spot_number)");
    query.bindValue(":spot_number", spot_number);

    if(!query.exec())
        qDebug() << "spotExists query failed: " << query.lastError();

    bool exists = query.first();

    if(exists)
        qDebug() << "Spot#" << spot_number << "already exists";

    return exists;
}


QVector<Spot> DBManager::getCurrentSpots()
{
    QVector<Spot> result;
    QSqlQuery query;
    query.prepare("select spot_number from spots");
    if(!query.exec())
        qDebug() << "getCurrentSpots query failed" << query.lastError();
    else
    {
        while(query.next())
        {
            Spot spot;
            spot.id_ = query.value("spot_number").toInt();
            result.push_back(spot);
            qDebug() << spot.id_;
        }
    }

    return result;

}


//Manage table "bookings"
bool DBManager::insertBooking(const Booking& booking) //Kan ikke lide at den her funktion både bruger
{                                                     //'success' til available spot og query check.
    bool success;
    if(!spotIsAvailableInPeriod(booking.spot_number_, booking.start_date_, booking.end_date_))
        success = false;
    else
    {
        QSqlQuery query;
        query.prepare("insert into bookings (spot_number, start_date, end_date, visitor_name) "
                      "VALUES (:spot_number, :start_date, :end_date, :visitor_name)");

        query.bindValue(":spot_number", booking.spot_number_);
        query.bindValue(":start_date", booking.start_date_.toString(Qt::DateFormat::ISODate));
        query.bindValue(":end_date", booking.end_date_.toString(Qt::DateFormat::ISODate));
        query.bindValue(":visitor_name", booking.visitor_name_);

        success = query.exec();
        if(!success)
            qDebug() << "Failed to insert booking" << query.lastError();
    }

    return success;
}

bool DBManager::spotIsAvailableInPeriod(int spot_number, QDate start_date, QDate end_date)
{
    QSqlQuery query;
    query.prepare("select start_date, end_date from bookings where spot_number == (:spot_number)");
    query.bindValue(":spot_number", spot_number);

    if(!query.exec())
        qDebug() << "Failed to retrieve booking dates" << query.lastError();

    while(query.next())
    {
        QDate existing_start_date = QDate::fromString(query.value("start_date").toString(), Qt::DateFormat::ISODate);
        QDate existing_end_date = QDate::fromString(query.value("end_date").toString(), Qt::DateFormat::ISODate);

        if(!(end_date <= existing_start_date || start_date >= existing_end_date))
        {
            qDebug() << "Spot" << spot_number << "not available in period" <<
                        start_date.toString(Qt::DateFormat::ISODate) << "-" << end_date.toString(Qt::DateFormat::ISODate);
            return false;
        }
    }
    return true;
}

bool DBManager::removeBooking(int booking_id)
{
    QSqlQuery query;
    query.prepare("delete from bookings where id == (:booking_id)");
    query.bindValue(0,booking_id);

    bool success = query.exec();
    if(!query.exec())
        qDebug() << "Failed to remove booking: " << query.lastError();
    return success;

}

QVector<Booking> DBManager::getCurrentBookings()
{
    QVector<Booking> result;
    QSqlQuery query;
    query.prepare("select * from bookings");

    bool success = query.exec();
    if(!success)
        qDebug() << "Failed to get current bookings:" << query.lastError();

    while(query.next())
    {
        Booking b;
        b.visitor_name_ = query.value("visitor_name").toString();
        b.spot_number_ = query.value("spot_number").toInt();
        b.start_date_ = QDate::fromString(query.value("start_date").toString());
        b.end_date_ = QDate::fromString(query.value("end_date").toString());

        result.push_back(b);
    }

    return result;
}

