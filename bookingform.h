#ifndef BOOKINGFORM_H
#define BOOKINGFORM_H

#include <QWidget>
#include <QDate>
#include <QString>



namespace Ui {
class BookingForm;
}

class BookingForm : public QWidget
{
    Q_OBJECT

public:
    explicit BookingForm(QWidget *parent = nullptr);
    ~BookingForm();

signals:
    void bookingComplete(QDate from, QDate to, QString visitor);

private slots:
    void on_buttonBox_accepted();

private:
    Ui::BookingForm *ui;
};

#endif // BOOKINGFORM_H
