#include "mainwindow.h"

#include <QApplication>
void populate_db();
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();

//    DBManager dbm("camp.db");
//    QVector<Spot> spots = dbm.getCurrentSpots();
//    populate_db();
    return 0;

}


#include <QDate>
#include <QDebug>

#include <QStringList>
#include "names.h"




void populate_db()
{
    DBManager dbm("camp.db");
    QDate date(QDate::currentDate());
    Booking b(1, names[0], date, date.addDays(-2));

    qsrand(QTime::currentTime().msec());

    for(int i = 0; i < 1000; i++)
    {
        int days_modifier = qrand() % 100;
        int length_of_stay = qrand() % 14 + 1;
        int spot_number = qrand() % 50 + 1;
        QString name = names[qrand() % names.length()];
        QDate start_date = QDate::currentDate().addDays(days_modifier);
        QDate end_date = start_date.addDays(length_of_stay);
        Booking b(spot_number, name, start_date, end_date);
        dbm.insertBooking(b);
    }

    for(int i = 1; i < 100; i++)
        dbm.insertSpot(i);

}
