#ifndef SPOTBUTTONSGRID_H
#define SPOTBUTTONSGRID_H

#include <QWidget>
#include <QGridLayout>
#include <QVector>
#include "spot.h"
#include "spotbutton.h"
#include "dbmanager.h"

class SpotButtonsGrid : public QWidget
{
   Q_OBJECT
public:
    SpotButtonsGrid();
    SpotButtonsGrid(DBManager* dbm, QWidget *parent = nullptr);
    ~SpotButtonsGrid();

private:
    QVector<SpotButton*> spot_buttons_;
    QGridLayout* layout;
    DBManager* dbm_;


};

#endif // SPOTBUTTONSGRID_H
